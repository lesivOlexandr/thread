import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(async () => res.send(await commentService.getCommentById(req.body.commentId)))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(() => res.send({ ok: true }))
    .catch(next));

export default router;
