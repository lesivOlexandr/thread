import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      negate = false
    } = filter;

    const where = {};
    if (userId && negate === 'false') {
      Object.assign(where, { userId });
    }
    if (userId && negate === 'true') {
      Object.assign(where, { userId: { [Op.ne]: userId } });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "comment"."deletedAt" IS NULL
                        AND "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "comment"."deletedAt" IS NULL
                        AND "post"."id" = "comment"."postId")
                        `), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            // TODO: refactor this
            [sequelize.literal(`
              (SELECT SUM(CASE WHEN "commentReactions"."isLike" = true THEN 1 ELSE 0 END)
              AS "likeCount" FROM "commentReactions"
              WHERE "commentReactions"."commentId" = "comments"."id"
              )`), 'likeCount'],
            [sequelize.literal(`
              (SELECT SUM(CASE WHEN "commentReactions"."isLike" = false THEN 1 ELSE 0 END)
              AS "dislikeCount" FROM "commentReactions"
              WHERE "commentReactions"."commentId" = "comments"."id"
              )`), 'dislikeCount']
          ]
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: []
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
