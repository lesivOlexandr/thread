import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: {
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount,
    id: commentId
  },
  userId,
  toggleEditedComment,
  deleteComment,
  likeComment,
  dislikeComment
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        <CommentUI.Action onClick={() => likeComment(commentId)}>
          <Icon name="thumbs up" />
          { likeCount || 0 }
        </CommentUI.Action>
        <CommentUI.Action onClick={() => dislikeComment(commentId)}>
          <Icon name="thumbs down" />
          { dislikeCount || 0 }
        </CommentUI.Action>
        {userId === user.id
          ? (
            <>
              <CommentUI.Action>
                <Icon name="edit" onClick={toggleEditedComment} />
              </CommentUI.Action>
              <CommentUI.Action onClick={() => deleteComment(commentId)}>
                <Icon name="trash" />
              </CommentUI.Action>
            </>
          )
          : null}
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  userId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
