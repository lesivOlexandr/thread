import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = (id, postData) => async (dispatch, getRootState) => {
  await postService.updatePost(id, postData);
  const updatedPost = await postService.getPost(id);

  const { posts: { posts, expandedPost, editedPost } } = getRootState();
  const updatePosts = posts.map(post => (post.id !== updatedPost.id ? post : { ...post, ...updatedPost }));

  dispatch(setPostsAction(updatePosts));

  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction(updatedPost));
  }
  if (editedPost && editedPost.id === updatedPost.id) {
    dispatch(setEditedPostAction(undefined));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updatePosts = posts.filter(post => post.id !== postId);

  dispatch(setPostsAction(updatePosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = post => async dispatch => {
  dispatch(setEditedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.likePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.dislikePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(request.id, request);
  const updatedComment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: (post.comments || []).map(comment => (comment.id === id ? updatedComment : comment))
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedComment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));
  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const deleteComment = id => async (dispatch, getRootState) => {
  await commentService.deleteComment(id);

  const filterComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: (post.comments || []).filter(comment => comment.id !== id)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== expandedPost.postId
    ? post
    : filterComments(post)));

  dispatch(setPostsAction(updated));
  dispatch(setExpandedPostAction(filterComments(expandedPost)));
};

export const likeComment = id => async (dispatch, getRootState) => {
  const updatedComment = await commentService.likeComment(id);
  const { posts: { expandedPost } } = getRootState();

  const mapComments = post => ({
    ...post,
    comments: (post.comments || []).map(comment => (comment.id === id ? updatedComment : comment))
  });
  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};

export const dislikeComment = id => async (dispatch, getRootState) => {
  const updatedComment = await commentService.dislikeComment(id);
  const { posts: { expandedPost } } = getRootState();

  const mapComments = post => ({
    ...post,
    comments: (post.comments || []).map(comment => (comment.id === id ? updatedComment : comment))
  });
  dispatch(setExpandedPostAction(mapComments(expandedPost)));
};
