import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Form, Button } from 'semantic-ui-react';
import { toggleEditedPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

const EditedComment = ({
  editedComment,
  updateComment: update,
  toggleComment: toggle
}) => {
  const { body: editedPostBody, id: commentId } = editedComment;
  const [body, setBody] = useState(editedPostBody || '');

  const handleEditComment = async () => {
    if (!body) {
      return;
    }
    await update({ id: commentId, body });
    toggle();
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {editedComment
        ? (
          <Modal.Content>
            <Form onSubmit={handleEditComment}>
              <Form.TextArea
                name="body"
                value={body}
                placeholder="Do ya wanna change something?"
                onChange={ev => setBody(ev.target.value)}
              />
              <Button color="blue" type="submit">Save edits</Button>
            </Form>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

EditedComment.propTypes = {
  editedComment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  toggleComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  editedPost: rootState.posts.editedPost
});

const actions = { toggleEditedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedComment);
