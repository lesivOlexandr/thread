import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import { toggleEditedPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';
import styles from './styles.module.scss';

const EditedPost = ({
  editedPost,
  updatePost,
  uploadImage,
  toggleEditedPost: toggle
}) => {
  const { body: editedPostBody, id: postId, editedImage = {} } = editedPost;
  const [body, setBody] = useState(editedPostBody || '');
  const [image, setImage] = useState({ imageLink: editedImage.link, imageId: editedImage.id });
  const [isUploading, setIsUploading] = useState(false);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await updatePost(postId, { imageId: image?.imageId, body });
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {editedPost
        ? (
          <Modal.Content>
            <Segment>
              <Form onSubmit={handleEditPost}>
                <Form.TextArea
                  name="body"
                  value={body}
                  placeholder="Do ya wanna change something?"
                  onChange={ev => setBody(ev.target.value)}
                />
                {image?.imageLink && (
                  <div className={styles.imageWrapper}>
                    <Image className={styles.image} src={image?.imageLink} alt="post" />
                  </div>
                )}
                <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                  <Icon name="image" />
                  Attach image
                  <input name="image" type="file" onChange={handleUploadFile} hidden />
                </Button>
                <Button floated="right" color="blue" type="submit">Save edits</Button>
              </Form>
            </Segment>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

EditedPost.propTypes = {
  editedPost: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  editedPost: rootState.posts.editedPost
});

const actions = { toggleEditedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedPost);
