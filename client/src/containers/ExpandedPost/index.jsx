import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  dislikePost,
  likePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import EditedComment from 'src/containers/EditedComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  userId,
  toggleEditedPost,
  deletePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updateComment: update,
  deleteComment: deleteUserComment,
  likeComment: likeUserComment,
  dislikeComment: dislikeUserComment
}) => {
  const [editedComment, setCommentBody] = useState(null);
  const toggleEditedComment = comment => {
    const editedCommentBody = editedComment ? null : comment;
    setCommentBody(editedCommentBody);
  };
  return (
    <>
      <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
        {post
          ? (
            <Modal.Content>
              <Post
                post={post}
                userId={userId}
                likePost={like}
                dislikePost={dislike}
                toggleEditedPost={toggleEditedPost}
                deletePost={deletePost}
                toggleExpandedPost={toggle}
                sharePost={sharePost}
              />
              <CommentUI.Group style={{ maxWidth: '100%' }}>
                <Header as="h3" dividing>
                  Comments
                </Header>
                {post.comments && post.comments
                  .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                  .map(comment => (
                    <Comment
                      key={comment.id}
                      comment={comment}
                      userId={userId}
                      toggleEditedComment={() => toggleEditedComment(comment)}
                      deleteComment={deleteUserComment}
                      likeComment={likeUserComment}
                      dislikeComment={dislikeUserComment}
                    />
                  ))}
                <AddComment postId={post.id} addComment={add} />
              </CommentUI.Group>
            </Modal.Content>
          )
          : <Spinner />}
      </Modal>
      {editedComment
        && (
          <EditedComment
            editedComment={editedComment}
            updateComment={update}
            toggleComment={toggleEditedComment}
            deleteComment={deleteUserComment}
          />
        )}
    </>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
