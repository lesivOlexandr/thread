import callWebApi from 'src/helpers/webApiHelper';

export const getAllUsers = async filter => {
  const response = await callWebApi({
    endpoint: '/api/users',
    type: 'GET',
    query: filter
  });
  return response.json();
};
